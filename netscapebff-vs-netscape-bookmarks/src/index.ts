/// <reference path="./index.d.ts" />
import { writeFileSync } from "fs";
import netscape from "netscape-bookmarks";
import { Bookmark, Folder, genBookmarkHTML } from "netscapebff";

// https://github.com/pxlprfct/bookmarked/blob/main/src/utils.ts
// https://bobbyhadz.com/blog/typescript-check-type-of-variable#using-a-type-predicate-to-check-the-type-of-a-variable
// const isFolder = (content: unknown): content is Folder => (content as Folder).children !== undefined;
const isBookmark = (content: unknown): content is Bookmark => (content as Bookmark).href !== undefined;

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce
// https://github.com/pxlprfct/bookmarked/blob/main/src/formatters/html.ts#L26
const convertSingleFolder = (folder: Folder) => {
    const contents = folder.children.reduce((accumulator: Record<string, string>, currentValue) => {
        if (isBookmark(currentValue)) {
            accumulator[currentValue.name] = currentValue.href;
        }
        return accumulator;
    }, {});
    // console.log(contents);

    const netscapeBookmarkFolder = { [folder.name]: { contents } };
    return netscapeBookmarkFolder;
};

// https://github.com/joaopalmeiro/netscapebff
// https://github.com/bahamas10/node-netscape-bookmarks/
const bookmarksNetscapebff = [
    {
        name: "Profiles",
        children: [
            {
                name: "GitHub",
                href: "https://github.com/joaopalmeiro",
            },
            {
                name: "GitLab",
                href: "https://gitlab.com/joaommpalmeiro",
            },
            {
                name: "Mastodon",
                href: "https://ciberlandia.pt/@joaopalmeiro",
            },
        ],
    },
];
const bookmarksNetscapeBookmarks = convertSingleFolder(bookmarksNetscapebff[0]);
// console.log(bookmarksNetscapeBookmarks);

writeFileSync("bookmarks_netscapebff.html", genBookmarkHTML(bookmarksNetscapebff));
writeFileSync("bookmarks_netscape_bookmarks.html", netscape(bookmarksNetscapeBookmarks));

console.log("All done!");
