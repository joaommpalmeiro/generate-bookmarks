# nodejs-d3-wretch-netscape-bookmarks

## Development

```bash
nvm install && nvm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

## Notes

- `npm install wretch netscape-bookmarks semver && npm install -D typescript ts-node @tsconfig/node18 prettier @feedzai/prettier-config @npm/types @types/semver`
- https://github.com/npm/registry/blob/master/docs/REGISTRY-API.md
- https://www.edoardoscibona.com/exploring-the-npm-registry-api + https://www.npmjs.com/package/query-registry
- https://httpie.io/download (available for Windows too)
- https://github.com/d3/d3 + https://github.com/d3/d3/blob/main/package.json + https://www.npmjs.com/package/d3
- `npm init -y`
- https://github.com/TypeStrong/ts-node/tree/v10.9.1 + https://github.com/tsconfig/bases/blob/main/bases/node18.json
- https://semver.npmjs.com/
- https://www.npmjs.com/package/semver
- https://www.npmjs.com/package/npm-registry-fetch
- https://github.com/npm/types + https://www.npmjs.com/package/@npm/types
- https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/npm + https://www.npmjs.com/package/@types/npm
- `cmp bookmarks.html "bookmarks copy.html"`
- https://github.com/gullwing-io/bookworms + https://www.npmjs.com/package/bookworms
- https://github.com/pxlprfct/bookmarked + https://github.com/pxlprfct/bookmarked/issues/32
- https://github.com/bahamas10/node-netscape-bookmarks
- https://www.infopedia.pt/apoio/artigos/$bookmark
- https://support.mozilla.org/en-US/questions/1319392: "You don't need to close the \<DT\> tags, but I recommend it so that if you view the HTML in a syntax highlighting editor, it will be well understood."
- There are two issues with the bookmarks generated for `d3@5.16.0`:
  - `d3-array`:
    - https://github.com/d3/d3-array/issues/86
    - Change `1.3.0` to `1.2.4`
  - `d3-selection`:
    - https://github.com/d3/d3-selection/issues/241#issuecomment-730033885
    - Change `1.4.2` to `1.4.1`
