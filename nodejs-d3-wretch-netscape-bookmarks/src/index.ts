/// <reference path="./index.d.ts" />
import { ManifestVersion, Packument } from "@npm/types";
import { writeFileSync } from "fs";
import netscape from "netscape-bookmarks";
import { maxSatisfying } from "semver";
import wretch from "wretch";

const BASE_URL_REGISTRY_API = "https://registry.npmjs.org";
const BASE_URL_GH = "https://github.com";
// const BASE_URL_D3_GH = `${BASE_URL_GH}/d3`;

// https://www.typescriptlang.org/docs/handbook/declaration-files/templates/module-d-ts.html#testing-your-types
// https://typestrong.org/ts-node/docs/troubleshooting/#missing-types
// https://www.typescriptlang.org/docs/handbook/triple-slash-directives.html
const main = async (pkg: string, version: string) => {
    // https://github.com/elbywan/wretch#motivation
    // https://github.com/elbywan/wretch#minimal-example
    // https://github.com/elbywan/wretch#response-types-
    // https://github.com/npm/types/blob/master/index.d.ts#L119
    // https://github.com/elbywan/wretch/issues/1
    const metadata: ManifestVersion = await wretch(`${BASE_URL_REGISTRY_API}/${pkg}/${version}`).get().json();

    const deps = metadata.dependencies;
    // console.log(deps);

    if (deps) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries
        // https://www.robcallaghan.co.uk/blog/asynchronous-javascript-fetch-promises-and-async-await/#Running-Promises-in-Parallel
        // https://dmitripavlutin.com/javascript-fetch-async-await/#5-parallel-fetch-requests
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/allSettled
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
        // https://javascript.info/promise-api
        // https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#arrays
        const requests: Promise<Packument>[] = Object.keys(deps).map((key) =>
            wretch(`${BASE_URL_REGISTRY_API}/${key}`).get().json(),
        );
        // console.log(requests);
        const results = await Promise.all(requests);
        // console.log(results);

        // const bookmarks = [];
        // https://www.typescriptlang.org/docs/handbook/utility-types.html#recordkeys-type
        const bookmarks: Record<string, string> = {};
        for (const result of results) {
            const pkgName = result.name;
            // https://github.com/npm/node-semver#ranges-1
            // https://www.npmjs.com/package/semver
            // console.log(result)

            const allVersions = Object.keys(result.time);
            // console.log(allVersions);
            const depVersion = deps[pkgName];
            // console.log(depVersion);

            const maxSatisfyingVersion = maxSatisfying(allVersions, depVersion);
            // console.log(maxSatisfyingVersion);

            if (maxSatisfyingVersion) {
                const name = `${pkgName}@${maxSatisfyingVersion}`;
                // const bookmark = {
                //     href: `${BASE_URL_D3_GH}/${pkgName}/tree/v${maxSatisfyingVersion}`,
                //     name,
                // };

                // bookmarks.push(bookmark);
                // https://developer.mozilla.org/en-US/docs/Web/API/URL/URL
                const repoURL = new URL(`/d3/${pkgName}/tree/v${maxSatisfyingVersion}`, BASE_URL_GH);
                bookmarks[name] = repoURL.toString();

                // console.log(repoURL, bookmarks[name]);
                console.log(`${name} ✔`);
            } else {
                console.log(
                    `There is no version available that satisfies ${pkgName}. It will be ignored in the final output.`,
                );
            }
        }

        // console.log(bookmarks);
        // https://github.com/pxlprfct/bookmarked#bookmarks-inside-folders
        // https://learn.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/aa753582(v=vs.85)
        // Bookmarks inside folders is not working as expected: https://github.com/pxlprfct/bookmarked/issues/32
        // const html = bookmarked([
        //     {
        //         name: `${pkg}@${version}`,
        //         children: bookmarks,
        //     },
        // ]);
        // console.log(html);
        // https://github.com/bahamas10/node-netscape-bookmarks#examples
        const html = netscape({
            [`${pkg}@${version}`]: {
                "contents": bookmarks,
            },
        });
        // console.log(html);

        // https://bobbyhadz.com/blog/typescript-write-to-a-file
        // https://nodejs.org/dist/latest-v18.x/docs/api/fs.html#fswritefilesyncfile-data-options
        writeFileSync("bookmarks.html", html);

        // const numberBookmarks = bookmarks.length;
        const numberBookmarks = Object.keys(bookmarks).length;
        console.log(`Created ${numberBookmarks} bookmarks from ${Object.keys(deps).length} dependencies.`);
    } else {
        console.log(`${pkg}@${version} has no dependencies.`);
    }
};

main("d3", "5.16.0");
// main("bookmarked", "2.0.7");
